#!/bin/bash

# Script de génération de certificats TLS (Let's Encrypt). Ce script n'est à utiliser que lors de la première génération des certificats TLS. 
# Il doit être lancé depuis le même emplacement que le docker-compose.prod.yml, ou bien l'emplacement de ce dernier doit ici être adapté.
# Source : https://github.com/wmnnd/nginx-certbot

# 1 - Vérification de l'existence de la commande docker-compose
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Erreur: docker-compose n''est pas installé.' >&2
  exit 1
fi

# 2 - Paramètres : domaine(s) concerné(s), adresse email (Let's Encrypt), taille de clé, chemin de sauvegarde (doit être cohérent avec les volumes montés dans docker-compose.prod.yml).
domains=(maximebn.com)
rsa_key_size=4096
data_path="./data/certbot"
email="tocomplete@tocomplete.com" # Adding a valid address is strongly recommended
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

# 3 - Vérification de l'existence d'un dossier du même nom
if [ -d "$data_path" ]; then
  read -p "Un dossier $data_path a été trouvé pour $domains. Continuer et écraser son contenu ? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
fi

# 4 - Téléchargement des confgurations TLS recommandées
if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Téléchargement des configurations TLS recommandées par Certbot (options-ssl-nginx.conf et ssl-dhparams.pem) ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

# 5 - Création de certificats temporaires pour le premier démarrage de Nginx
echo "### Création de certificats temporaires pour $domains ..."
path="/etc/letsencrypt/live/$domains"
mkdir -p "$data_path/conf/live/$domains"
docker-compose -f docker-compose.prod.yml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path/privkey.pem' \
    -out '$path/fullchain.pem' \
    -subj '/CN=localhost'" certbot
echo

# 6 - Démarrage de Nginx
echo "### Démarrage de nginx ..."
docker-compose -f docker-compose.prod.yml up --force-recreate -d nginx
echo

# 7 - Suppression des certificats temporaires
echo "### Suppression des certificats temporaires pour $domains ..."
docker-compose -f docker-compose.prod.yml run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/$domains && \
  rm -Rf /etc/letsencrypt/archive/$domains && \
  rm -Rf /etc/letsencrypt/renewal/$domains.conf" certbot
echo

# 8 - Demande/génération des certificats Let's Encrypt
echo "### Demande de certificat Let's Encrypt pour $domains ..."

domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

docker-compose -f docker-compose.prod.yml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo

# 9 - Rechargement de Nginx
echo "### Nginx en cours d'actualisation ..."
docker-compose -f docker-compose.prod.yml exec nginx nginx -s reload