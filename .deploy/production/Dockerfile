# Multi-staged build : builder
# debian buster ditrib
FROM python:3.8.5-slim-buster as builder

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies + git + bash
RUN apt-get update \ 
    && apt-get install \
    && apt-get install -y gcc libpq-dev musl-dev \
    && apt-get install -y python-psycopg2 \
    && apt-get install -y git bash \
    && git --version && bash --version

# install dependencies
COPY ./requirements.txt .
RUN pip3 install -U pip \
   && pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt

# Multi-staged build : final image
# debian buster ditrib
FROM python:3.8.5-slim-buster

# create the appropriate directories
ENV APP_HOME=/home/app/
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/static
RUN mkdir $APP_HOME/media
WORKDIR $APP_HOME

# create the app user
# RUN addgroup app && adduser app --ingroup app

# install dependencies
RUN apt-get update && apt-get install -y libpq-dev
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN pip3 install --no-cache /wheels/*

# copy project
COPY . $APP_HOME

# chown all the files to the app user
# RUN chown -R app:app $APP_HOME

# change to the app user
# USER app