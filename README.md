<a href="https://pypi.org/"><img src="https://img.shields.io/badge/Package%20Manager-pip3-brightgreen?style=flat-square" alt="Yarn Version" /></a>
<a href=""><img src="https://img.shields.io/badge/license-AGPL%20v3-yellow?style=flat-square" alt="License" /></a>

[![pipeline status](https://gitlab.com/maximebn/maximebn-photography/badges/release/dockerize/pipeline.svg?style=flat-square)](https://gitlab.com/maximebn/maximebn-photography/-/commits/release/dockerize)
[![coverage report](https://gitlab.com/maximebn/maximebn-photography/badges/release/dockerize/coverage.svg?style=flat-square)](https://gitlab.com/maximebn/maximebn-photography/-/commits/release/dockerize)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg?style=flat-square)](https://github.com/PyCQA/bandit)

<p align="center">Une application construite sur <a href="https://www.python.org/" target="blank">Python</a> et <a href="https://www.djangoproject.com/" target="blank">Django</a>. Un exemple présentant mon travail photographique : <a href="https://maximebn.com" target="blank">maximebn.com</a></p>
    <p align="center">
</p>

## Features 🔥

- Features relatives au framework Django : création d'utilisateurs, admin etc,
- Stories : projets photograhiques,
- Blog : articles divers,
- Formulaire de contact,

## Prérequis 🗿

Ce projet a été dockerisé. En développement comme en production, il est nécessaire que Docker soit installé sur la machine hôte.

- [Docker Engine](https://docs.docker.com/engine/install/ubuntu/#install-docker-engine) : > v.19.03.11+
- [Docker Compose](https://docs.docker.com/compose/install/#install-compose) : > v.1.26.0+
- [VSCode Remote Pack Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- [Git](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- Un compte GitLab :)

## Utilisation 🦄

### Configuration globale de Git

Informations utilisateur :

```bash
git config --global user.name "Maximus"
git config --global user.email "maxi@mus.com"
```

C'est optionel, la configuration des _credentials_. Ils peuvent être enregistrés par _git_ si l'on veut éviter de les saisir plusieurs fois dans le _devContainer_. En local :

```bash
git config --global credential.helper 'store'
```

### Configurer les variables d'environnement

Les informations sensibles, comme les noms d'utilisateurs, mots de passe, configurations mails etc ne devraient jamais être affichées en clair dans l'un des fichiers de configuration. Il faut utiliser des variables d'environnement. Ici, la librairie `python-decouple` est utilisée à cette fin mais l'on peut très bien s'en passer. Aucune variable d'environnement n'est configurée par défaut -> créer un fichier `settings.ini` à la racine du projet et de le paramétrer convenablement. Pour plus d'informations :

https://docs.djangoproject.com/fr/2.1/topics/settings/

https://pypi.org/project/python-decouple/

Les choses à savoir pour ce projet sont :

- Les variables d'environnement doivent être centralisées dans un fichier .env situé à la racine du dossier maximebn. Ce fichier regroupe plusieurs variables, utiles pour les environnements de développement comme de production,
- Ce fichier doit respecter la structure, tout au moins le nommage, suivant :

```bash
[settings]
SECRET_KEY_LOCAL=clé secrète application Django dev
SECRET_KEY_PROD=clé secrète application Django production
DEBUG_LOCAL=True
DEBUG_PROD=False

# Database parameters in local environment
DB_ENGINE_LOCAL=https://docs.djangoproject.com/fr/3.2/ref/databases/#postgresql-notes
DB_NAME_LOCAL=nom de la base de données dev
DB_USER_LOCAL=utilisateur de la base de données dev
DB_PASSWORD_LOCAL=mot de passe utilisateur base de données dev
DB_HOST_LOCAL=hôte

# Database parameters in production environment : to me modified
DB_ENGINE_PROD=https://docs.djangoproject.com/fr/3.2/ref/databases/#postgresql-notes
POSTGRES_DB=nom de la base de données prod
POSTGRES_USER=utilisateur de la base de données prod
POSTGRES_PASSWORD=mot de passe utilisateur base de données prod

# Email backend configuration parameters
EMAIL_BACKEND=https://docs.djangoproject.com/en/3.2/topics/email/#topic-email-backends
EMAIL_HOST=hôte
EMAIL_USE_TLS=True
EMAIL_PORT=587
EMAIL_HOST_USER=utilisateur (adresse mail)
EMAIL_HOST_PASSWORD=mot de passe
SERVER_EMAIL=adresse mail

# Administration parameters for emailing
ADMIN_NAME=nom administrateur
ADMIN_EMAIL=email administrateur
ADMIN_PASSWORD=mot de passe administrateur

RECAPTCHA_SITE_KEY=https://www.google.com/recaptcha/about/
RECAPTCHA_SECRETKEY=https://www.google.com/recaptcha/about/
```

### En développement : devContainers

Pour simplifier la montée d'un environnement de développement de ce projet, la fonctionnalité devContainers de VS est utilisée. Voir la documentation officielle :

https://code.visualstudio.com/docs/remote/containers

L'idée est de pouvoir utiliser un container _Docker_ au sein de _VSCode_ comme environnement de développement. Aucune des fonctionnalités de VSCode n'est perdue, mais surtout, il suffit de récupérer le projet, de l'ouvrir dans VScode qui, en détectant le fichier de configuration `devcontainer.json`, proposer automatiquement de construire le container et de développer directement à l'intérieur de celui-ci. La première fois, l'étape de construction du container (voir `docker-compose.yml`) peut être longue, tout comme celle d'installation des extensions pour _VSCode_ (le fichier de configuration qu'on vient d'aborder permet également de préciser quelles extensions l'on souhaite voir installées par défaut dans notre éditeur). Par la suite, Docker utilise un cache.

Il n'y a rien de plus à faire pour pouvoir démarrer le projet en développement !

## Déploiement en production 💣

### Prérequis

A l'exception de l'extension VSCode _Remote Pack Extension_, tous les prérequis mentionnés dans le paragraphe #2 sont nécessaires. Il convient également d'avoir un accès ssh à une machine distante.

Si souhaité, un utilisateur de déploiement peut également être créé, c'est ce qui a été fait ici :

```bash
sudo adduser deployer
```

### Intégration et déploiement continu

L'installation en production est en partie automatisée. Elle repose essentiellement sur Docker-Compose et Gitlab CI.

La configuration de l'intégration et du déploiement continu est décrite dans le ficher _.gitlab-ci.yml_ à la racine du projet. Pour fonctionner, celui-ci a besoin de plusieurs variables d'environnement qui doivent être décrites à partir des paramètres du projet (Settings > CI/CD > Variables). Ces variables sont par exemple l'adresse IP du serveur sur lequel doit être déployé l'application, des identifiants du serveur SMTP etc. Les variables nécessaires ne sont pas listées ici mais vous pouvez contacter l'auteur pour en savoir plus.

Chaque _push_ sur la branche master (production) entraîne différentes étapes d'intégration et déploiement en production.

### Spécificité de la première mise en production

Lors de la première installation, il est nécessaire après le premier push sur la branche master de lancer le script _init-letsencrypt.sh_ à partir de la machine hôte. Ce script permet de générer de faux certificats TLS de manière à ce que Nginx puisse démarrer. Une fois Nginx démarré, ces faux certificats sont supprimmés, puis des certificats LetsEncrypt générés de manière à obtenir une connexion HTTPS certifiée. Il n'y a rien de plus à faire. Pour plus de détails, se reporter au paragraphe suivant sur le fonctionnement de l'application.

### Bonus

Les éventuelles évlution et mises à jour des différentes versions entraîneront la création et le stockage, à terme, de nombreuses images Docker (entre autres). Afin d'éviter des opérations de maintenance manuelles, on peut planifier un nettoyage de divers éléments inutilisés de cette façon sur une machine Linux, ici tous les jours à 3h00 :

```bash
crontab -e
0 3 * * * usr/bin/docker system prune -a -f
```

Voir la documentation correspondante si besoin (https://docs.docker.com/engine/reference/commandline/system_prune/).

## Fonctionnement 👾

Dans les pararaphes qui suivent, est décrit succinctement le fonctionnement de divers aspects de l'application.

### Gestion des dépendances

De nombreuses dépendances sont utilisées dans l'application et spécifiées dans les fichier `requirements.txt` et `requirements-dev.txt`, à mettre à jour lors d'ajout de nouvelles librairies externes.

Les dépendances sont mises à jour au moyen d'un scan automatique fourni par un service tiers (renovate). Pour mettre à jour la liste des packages :

```bash
pip freeze > requirements.txt
```

Dans un environnement local, utiliser l'option --local :

```bash
pip freeze --local > requirements-dev.txt
```

A chaque nouvelle installation d'un package, il est nécessaire de mettre à jour la liste des packages listés dans un fichier _requirements.txt_ afin que le bot puisse scanner une version à jour de ce fichier et proposer d'éventuelles mises à jour sur l'intégralité des dépendances utilisées dans le programme.

### Image Docker de maximebn-photography

Deux images Dockers peuvent être construites : pour l'environnement de développement et de production. Le `DockerFile` de production se situe dans le dossier .deploy/production. Il permet, en résumé, de construire une image basée sur Debian-buster, d'y installer toutes les dépendances nécessaires au fonctionnement de l'application Django et d'y copier l'ensemble du projet.

Le `DockerFile` de développement se situe dans le dossier .deploy/dev. Il est relativement similaire si ce n'est qu'il n'utilise pas de build multi-étapes. Plus d'informations consultables ici : https://docs.docker.com/develop/develop-images/multistage-build/

### Docker-Compose

Deux fichiers, `docker-compose.yml` et `docker-compose.prod.yml`, définissent et permettent de lancer plusieurs conteneurs, lesquels sont perçus comme des services nécessaires au fonctionnement de l'application. Parmi ces services, l'application Django dont l'image est construite à partir des `DockerFile` décrits ci-dessus.

#### Développement

Trois services sont définis dans `docker-compose.yml` pour les environnements de développements :

- web : l'application Django
- db : un serveur de base de données, PostreSQL
- pgadmin : un outil (interface graphique) pour la gestion et l'administration du serveur ci-dessus

Ci-dessous, une description de chacun des services :

##### Service web

```bash
// Image Docker à utiliser pour construire ce service. Ici, la dernière version de maximebn-photgraphy
image: maximebn-photography:latest

// Nom du conteneur
container_name: maximebn-photography

// Politique de redémarrage du conteneur. Ici, systématique (https://docs.docker.com/config/containers/start-containers-automatically/)
restart: always

// Définition des variables environnement nécessaires (https://docs.docker.com/compose/environment-variables/)
environment:
    // Django : quel fichier utiliser pour la configuration du programme, ici le fichier spécifique à un environnement de développement. Variable environnement propre à Django.
    DJANGO_SETTINGS_MODULE: maximebn.settings.settings-local
    // Hôte base de données. Variable environnement propre à Django.
    DB_HOST: db

// Build du service (https://docs.docker.com/compose/reference/build/)
build:
    // Contexte : chemin/répertoire contenant le fichier Dockerfile nécessaire
    context: .
    dockerfile: .deploy/dev/Dockerfile

// Commandes à exécuter, spécifique à Django : migrations de DB, et lancement du serveur (https://docs.djangoproject.com/fr/3.2/topics/migrations/)
command: bash -c "python3 manage.py makemigrations --noinput && python3 manage.py migrate && python3 manage.py runserver 0.0.0.0:8000"

// Volumes : le conteneur arrêté, certaines données doivent être persistées sur la machine hôte.
volumes:
  - ./:/maximebn-photography

// Ports
 ports:
    - "8000:8000"

// Dépendance : ce service web attend que le service db soit healthy, cette condtion étant définie dans la description du service db et équvaut au serveur de DB démarré et prêt.
depends_on:
   db:
    condition: service_healthy
```

##### Service db

```bash
// Image Docker à utiliser pour construire ce service. Ici, la version 11-alpine de postgres
image: postgres:11-alpine

// Nom du conteneur
container_name: db

// Définition des variables environnement nécessaires (https://docs.docker.com/compose/environment-variables/)
environment:
    POSTGRES_DB: devmaximebn
    POSTGRES_PASSWORD: postgres
    POSTGRES_USER: postgres

// Politique de redémarrage du conteneur. Ici, systématique sauf si arrêt (https://docs.docker.com/config/containers/start-containers-automatically/)
restart: unless-stopped

// Volumes : le conteneur arrêté, les données doivent être persistées sur la machine hôte.
volumes:
    - /db-data/:/var/lib/postgresql/data

// Test de santé : comande pg_isready vérifiant le statut de connexion du serveur
// https://docs.postgresql.fr/11/app-pg-isready.html
// https://docs.docker.com/compose/compose-file/compose-file-v3/#healthcheck
healthcheck:
    test: "pg_isready -U postgres"
    interval: 5s
```

##### Service pgadmin

La définition du conteneur pgadmin est basique :

```bash
image: dpage/pgadmin4
restart: always
environment:
    PGADMIN_DEFAULT_EMAIL: pgadmin
    PGADMIN_DEFAULT_PASSWORD: pgadmin
    PGADMIN_LISTEN_PORT: 5050
ports:
    - 5051:5050
```

#### Production

##### Service web

```bash
// Quelle image utiliser pour le service web ? La dernière version de maximebn-photography, laquelle se trouve dans le registre des conteneurs gitlab. Ceci est réalisé pendant le déploiement continu détaillé dans une section à part.
image: registry.gitlab.com/maximebn/maximebn-photography:latest

// Nom du conteneur
container_name: maximebn-photography

// Quel DockerFile utiliser ?
build:
    context: .
    dockerfile: .deploy/production/Dockerfile

// Commande : ici, on utilise un serveur web HTTP Gunicorn plutôt que lse serveur web intégré à Django, voir la section consacrée à Gunicorn
command: bash -c "gunicorn maximebn.wsgi:application --bind 0.0.0.0:8000"

// Variables environnement
environment:
    DJANGO_SETTINGS_MODULE: maximebn.settings.settings-prod
    DB_HOST: db

// Volumes : static (javascript, assets) et media (uploads utilisateurs)
volumes:
    - static_volume:/home/app/static
    - media_volume:/home/app/media

// Fichier environnement
env_file:
    - maximebn/.env
expose:
    - 8000
depends_on:
    - db

// Particularité : il est nécessaire de réaliser les migrations de base et de créer automatiquement un administrateur Django lors de son déploiement. Voir la commande Django initadmin dans maximebn/management/commands.
entrypoint: ./.deploy/production/entrypoint.sh
```

##### Service db

Aucune particularité concernant le serveur de base de données :

```bash
image: postgres:11-alpine
container_name: db
restart: always
volumes:
    - /db-data/:/var/lib/postgresql/data
env_file:
    - maximebn/.env
```

##### Service nginx

```bash
image: nginx:1.18

// Connexions HTTP et HTTPS
ports:
    - "80:80"
    - "443:443"
restart: always

// Volumes :
- fichiers statiques et médias uploadés
- fichier de configuration de Nginx, qui se situe dans .deploy/nginx/conf.d, voir section correspondante
- certificats et clés LetsEncrypt, les volumes sont montés à l inverse des volumes montés dans le service Certbot.
volumes:
    - static_volume:/home/app/static
    - media_volume:/home/app/media
    - ./.deploy/nginx/conf.d/nginx.conf:/etc/nginx/conf.d/default.conf
    - ./data/certbot/conf:/etc/letsencrypt
    - ./data/certbot/www:/var/www/certbot
depends_on:
    - web

// Rechargement de nginx toutes les 6h. Ceci afin de permettre à Nginx de prendre en compte de nouveaux certificats TLS dans le cas où ils auraient été renouvelés (sans avoir à le faire manuellement donc)
command: '/bin/sh -c ''while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g "daemon off;"'''
```

##### Service certbot

Ce service utilise une image Docker Certbot. Let’s Encrypt valide le domaine, si l'opération est correctement réalisée, des données sont générées par Cerbot (certificats etc), lesquelles sont donc accessibles au sein du conteneur Certbot. Ces données sont nécessaires à Nginx qui doit pouvoir servir ces fichiers. Afin qu'ils soient accessibles, deux volumes sont donc montés (inversement dans le service nginx).

Une commande est également définie afin de vérifier, chaque 12h, que les certificats actuels ne doivent pas être renouvelés, et soient donc renouvelés si c'est le cas.

```bash
image: certbot/certbot:latest
volumes:
    - ./data/certbot/conf:/etc/letsencrypt
    - ./data/certbot/www:/var/www/certbot
entrypoint: "/bin/sh -c 'trap exit TERM; while :; do certbot renew; sleep 12h & wait $${!}; done;'"
```

### Intégration continue

Les différentes étapes d'intégration et déploiement continue sont décrites dans le fichier `.gitlab-ci.yml` à la racine du projet.

<p align="center">
	<img src=".documentation/gitlab_ci.png" align="center" alt="Intégration continue - stages" />
</p>

Il faut noter que les différentes étapes sont exécutées dans un container contenant certains outils Docker préinstallés, dans lequel on peut réaliser différentes opérations (dind : Docker in Docker). Plus d'informations : https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

#### Analyse statique

- Bandit : c'est un outil destiné à trouver d'éventuelles failles de sécurité communes : https://bandit.readthedocs.io/en/latest/
- Flake8 : c'est un outil destiné à la validation du code Python au regard de la PEP8 : https://flake8.pycqa.org/en/latest/
- Radon : c'est un outil permettant de générer diverses statistiques/métriques au regard du code analysé. Il est ici utilsé pour calculer la complexité cyclomatique, laquelle ne doit en moyenne pas dépasser un certain score : https://pypi.org/project/radon/

#### Tests unitaires

Les tests unitaires de **Django** utilisent le module `unittest` de la bibliothèque Python standard. Ce module définit les tests selon une approche basée sur des classes. Est également utilisée une librairie externe de mesure de couverture de code des programmes Pyhton, Coverage (https://coverage.readthedocs.io/en/coverage-5.5/.).

https://docs.python.org/3/library/unittest.html#module-unittest

La réalisation des tests unitaires nécessite de construire l'image de l'application, laquelle repose du docker-compose qui n'est pas installé par défaut avec docker-dind (Docker in Docker). Durant cette étape, docker-compose est donc installé manuellement, les différents services nécessaires au fonctionnement de l'application en mode développement sont buildés et démarrés, et enfin les test unitaires sont lancés.

Enfin, les tests unitaires ne sont réalisés que sur la branche master, le développeur doit s'assurer de tester son code en développement.

#### Build

Il s'agit de l'étape qui permet, à partir du `DockerFile` décrit pour l'environnement de production, de produire une image Docker de l'application et de la pousser au sein du registrer Docker de Gitlab.

Le build n'est réalisé que sur la branche master.

#### Release

Cette étape est très similaire à la précédente et consiste à tagger la version précédement poussée.

La release n'est réalisée que sur la branche master.

#### Déploiement

Enfin, la dernière étape, si toutes les précédentes se sont correctement déroulées, consiste à déployer le code en production :

- Connexion ssh au serveur distant,
- Récupération de la dernière version du code,
- Récupération de la dernière image Docker de l'application,
- Création et démarrage de tous le services nécessaires à l'application en production.

### Nginx

#### Configuration

Le serveur **Nginx** intercepte les requêtes HTTP pour les envoyer au couple (**Gunicorn** - **Django**). Ici est adoptée une configuration en reverse-proxy. **Nginx** est utilisé en serveur frontal pour servir des fichiers statiques et renvoyer la connexion au serveur **WSGI**. Elle est décrite dans le fichier `nginx.conf` dans le dossier .deploy/nginx/conf.d. La configuration est classique et la seule subtilité concerne l'ajout manuel des emplacements des certificats TLS que Nginx doit pouvoir servir.

#### Configuration de Nginx pour la virtualisation du `robots.txt`

Afin de demander à Google et autres moteurs de recherche quelles parties de votre application les robots doivent scanner pour l'indexation des pages, il est d'usage de faire appel à un fichier `robots.txt` de la forme :

```bash
User-agent: *
Disallow: /
```

**Nginx** propose de répondre un fichier virtuel `robots.txt` sans avoir réellement besoin de le créer physiquement sur le serveur. Cette étape est évidemment optionnelle, mais si vous souhaitez personnaliser l'indexation de votre application, vous pouvez ajouter ces lignes, à adapter, à la configuration de **Nginx** :

```bash
location /robots.txt {
    return 200 "User-agent: Googlebot-Image Disallow: / ";
}
```

Dans ce cas, on demande à tous les robots de n'indexer aucune image.

#### Configuration de Nginx pour limiter les appels par adresse IP

Le serveur peut être la cible d'appels répétés non souhaités pour diverses raisons (attaques, scans). Afin de limiter ces appels, il est possible de configurer Nginx de manière à définir un nombre maximum de requêtes autorisées par adresse IP dans un certain intervalle de temps. Ici, ce sont 5 requêtes par seconde :

```bash
// $binary_remote_addr spécifie un enregistrement des requêtes par adresse IP, zone est le nom de limit_req_zone, et rate le taux.
limit_req_zone $binary_remote_addr zone=one:20m rate=5r/s;

// Ici, on indique que la zone appelée one doit être utilisée pour limiter les requêtes.
server {
    limit_req   zone=one  burst=5 nodelay;
    # ...
}
```

Pour plus de détails, consulter ce lien : https://rogs.me/2020/04/secure-your-django-api-from-ddos-attacks-with-nginx-and-fail2ban/

#### Configuration de Nginx pour ne pas traiter les requêtes invalides (noms de domaine invalides)

Django est configuré pour alerter l'administrateur en cas de requête invalide. Les erreurs issues de requêtes refusées pour nom de domaine invalides (ceux étant autorisés sont définis dans le paramètre ALLOWED_HOSTS de Djang) sont nombreuses et causées par des bots utilisant de faux noms de domaines ou l'adresse IP du serveur. Il convient, pour éviter de recevoir de trop nombreux mails d'erreurs, de demander à Nginx de ne pas traiter ce type de requête invalide, de manière à ce que Django ne puisse pas les traiter et donc ne pas envoyer de mail d'erreur dans ce cas précis :

```bash
server {
    listen 443 ssl default_server;
    server_name _;
    return 444;
}
```

444 étant un code, non-standard, utilisé par Nginx pour fermer la connexion, ici quand l'hôte n'est pas celui attendu sur le serveur HTTPS par défaut.

Pour plus d'informations à ce sujet, consulter ce lien : https://nginx.org/en/docs/http/request_processing.html#how_to_prevent_undefined_server_names

#### Servir les fichiers statiques en production

Par défaut, c'est **Nginx** qui se charge de servir les fichiers statiques en production. La configuration nécessaire est décrite dans la section précédente. Il est également possible d'utiliser **WhiteNoise**, en association avec un **CDN** (_Content Delivery Network_) ou non. **WhiteNoise** permet d'éviter des configurations externes pour servir les fichiers statiques du projet. Il a l'avantage, lorsqu'il est utilisé avec **Django**, de proposer de grandes facilités d'installation et utilisation. C'est surtout vrai pour des projets plus conséquents. Il permet également de servir du contenu compressé au format `Brotli`.

Si l'on ne désire pas utiliser **WhiteNoise**, il n'y a rien de plus à faire.

### Gunicorn

**Gunicorn** est un serveur web **HTTP** **WSGI** (**Web Server Gateway Interface** - spécification qui définit une interface entre des serveurs et des applications web pour le langage **Python**) disponible pour **Unix**. C'est la configuration par défaut qui a ici été adoptée.

## Autres infos : gestion du cache 🙈

Bien que non nécessaire ici, une gestion du cache basique a été implémentée dans ce _template_ **Django**. Une solution courante, rapide et fiable est d'utiliser **MemCached**. L’outil va stocker directement les valeurs de cache dans la mémoire vive afin de garantir un temps d’accès optimal. De plus, l’architecture distribuée de **MemCached** permet d’utiliser des machines distantes afin de répartir la mise en cache de manière automatisée.

### Installation du binding Python

Plusieurs _bindings_ python sont disponibles sur **PyPI**. On peut par exemple installer **python-memcached** qui est référencé dans la documentation de Django.

```bash
sudo pip install python-memcached
```

### Gestion du cache

Une manière courante d’utiliser l’infrastructure de cache est de mettre en cache le résultat de vues individuelles. C'est la solution qui a ici été choisie.`django.views.decorators.cache` définit un décorateur `cache_page` qui se charge de mettre en cache la réponse de la vue. Il est simple à mettre en place :

```python
from django.views.decorators.cache import cache_page

@cache_page(60 * 10)
def my_view(request):
	...
```

`cache_page` accepte un seul paramètre : le délai d’expiration du cache en secondes. Dans l’exemple ci-dessus, le résultat de la vue `my_view()` reste en cache pendant 15 minutes (remarquez que nous avons écrit `60 * 10` par souci de lisibilité, le résultat donne `600`, c’est-à-dire 10 minutes multipliées par 60 secondes).

Pour plus d'information sur la gestion du cache dans Django, se référer à la documentation officielle : https://docs.djangoproject.com/fr/2.2/topics/cache/

## Licence 🔓

Sous licence GNU AGPL v.3.

## Liens utiles 📖

En dehors des références déjà explicitées dans cette documentaton, ces liens peuvent être utiles :

https://www.djangoproject.com/

https://www.python.org/

https://pypi.org/project/pip/

https://pyup.io/

https://developer.mozilla.org/fr/docs/Learn/Server-side/Django

https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment

https://www.digitalocean.com/community/tutorials/how-to-build-a-django-and-gunicorn-application-with-docker

https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/
