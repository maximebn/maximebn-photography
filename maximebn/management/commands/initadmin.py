from decouple import config
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Création du compte administrateur"

    def handle(self, *args, **options):
        User = get_user_model()
        if not User.objects.filter(username=config('ADMIN_NAME')).exists():
            User.objects.create_superuser(username=config('ADMIN_NAME'),
                                          email=config('ADMIN_EMAIL'),
                                          password=config('ADMIN_PASSWORD'))

        else:
            print('Compte administrateur déjà existant - il ne sera pas créé')
