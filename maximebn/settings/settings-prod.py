from .settings import *
from decouple import config
import re


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY_PROD')
DEBUG = config('DEBUG_PROD', default=False, cast=bool)

INSTALLED_APPS += [
    'gunicorn',
]

STATIC_ROOT = normpath(join(SITE_ROOT, 'static'))

# Allowed hosts for production environment, must be suitable value
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/
ALLOWED_HOSTS = ['51.15.242.185', '.maximebn.com']

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': config('DB_ENGINE_PROD'),         # Database engine
        'NAME': config('POSTGRES_DB'),             # Database name
        'USER': config('POSTGRES_USER'),
        'PASSWORD': config('POSTGRES_PASSWORD'),
        'HOST': config('DB_HOST_LOCAL'),
        'PORT': '5432',
        'TEST': {
            'NAME': 'maximebnPhotography',
        },
    }
}

# Cache configuration
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND':  'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 600,
        'OPTIONS': {
            'MAX_ENTRIES': 100
        }
    }
}

# -------------------------------------------------------------------------------
# Additionnal security settings
# ------------------------------------------------------------------------------

# Sensitive data exposure
# https://docs.djangoproject.com/en/2.1/ref/middleware/#http-strict-transport-security

# SECURE_HSTS_SECONDS = 3600 # 1 heure,to me modified after validation
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True
# SECURE_HSTS_PRELOAD = True
# SECURE_SSL_REDIRECT= True

# Click-jacking protection : X-frame middleware setting header option to deny loading resource within a frame
# https://docs.djangoproject.com/en/2.1/ref/clickjacking/

# X_FRAME_OPTIONS = 'DENY'

# XSS filtering protection enabled on web browsers (+ Django templates escaping specific characters)
# https://docs.djangoproject.com/en/2.1/ref/clickjacking/

# SECURE_BROWSER_XSS_FILTER  = True

# Cookie sessions protection, forcing cookies to be shared by HTTPS
# https://docs.djangoproject.com/en/2.1/topics/http/sessions/

SESSION_COOKIE_SECURE = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Information: PyUp tracking and deploying dependencies vulerabilities fixes automatically.
# https://pyup.io/

IGNORABLE_404_URLS = (
    re.compile(r'^/favicon\.ico$'),
    re.compile(r'^/robots\.txt$'),
)
